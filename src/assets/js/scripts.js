 function responsiveMenu() {
            $('.menuTitle').click(function () {
                $('.menu').slideToggle("slow");
                $('.menu').css({
                    "max-height": "800px"
                });
            });
        };

        $(function () {
            $(".menu").css({
                "display": "none",
                "transition": "none",
                "max-height": "inherit"
            });
            $("#toggleMenu").remove();
            responsiveMenu();
        });

        $(document).ready(function () {
            $('#clientesCarousel').carousel({
                interval: 5000
            });
            $('#sliderFashion').carousel();


            var $searchTrigger = $('[data-ic-class="search-trigger"]'),
                $searchInput = $('[data-ic-class="search-input"]'),
                $searchClear = $('[data-ic-class="search-clear"]');

            $searchTrigger.click(function () {

                var $this = $('[data-ic-class="search-trigger"]').parent();
                $this.toggleClass('active');
                $searchInput.focus();

            });

            $searchInput.blur(function () {

                if ($searchInput.val().length > 0) {

                    return false;

                } else {

                    $searchTrigger.removeClass('active');

                }

            });

            $searchClear.click(function () {
                $searchInput.val('');
                $searchTrigger.parent().toggleClass('active');
            });


        });