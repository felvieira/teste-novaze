module.exports = function (grunt) {

	// Grunt Config
	grunt.initConfig({

		// Copiar Arquivos
		copy: {
			public: {
				cwd: 'src',
				src: '**',
				dest: 'dist',
				expand: true
			},

			/*	public: {
					files: [{
							src: 'src/index.html',
							dest: 'dist/index.html'
						},
						{
							src: 'src/assets/css/*.css',
							dest: 'dist/assets/css/index.css'
						},
					],
				}*/
		},

		//limpar arquivos
		clean: {
			dist: {
				src: 'dist'
			}
		},

		//preparar os arquivos para minificacao e concatenacao css js
		useminPrepare: {
			html: 'dist/**/*.html'
		},

		usemin: {
			html: 'dist/**/*.html',
			css: 'dist/**/*.css'
		},

		imagemin: {
			public: {
				expand: true,
				cwd: 'dist/assets/img',
				src: '**/*.{png,jpg,gif}',
				dest: 'dist/assets/img'
			}
		},

		cssmin: {
			target: {
				files: [{
					expand: true,
					cwd: 'dist/assets/css',
					src: ['*.css', '!*.min.css'],
					dest: 'dist/assets/css',
					ext: '.min.css'
				}]
			}
		},


		// versionamento de arquivos imagens versoes cache
		rev: {
			options: {
				encoding: 'utf8',
				algorithm: 'md5',
				length: 8
			},

			imagens: {
				src: ['dist/assets/img/**/*.{png,jpg,gif}']
			},

			minificados: {
				src: ['dist/assets/js/**/*.min.js', 'dist/assets/css/**/*.min.css']
			}
		},

		sass: {
			dist: {
				files: [{
					expand: true,
					cwd: 'src/assets/sass',
					src: ['*.scss'],
					dest: 'src/assets/css',
					ext: '.css'
				}]
			}
		},

		browserSync: {
			public: {
				bsFiles: {
					src: ['src/**/*']
				},
				options: {
					watchTask: true,
					server: {
						baseDir: "src"
					}
				}
			}
		},

		watch: {

			sass: {
				files: 'src/assets/sass/**/*.scss',
				tasks: 'sass'
			},

			js: {
				options: {
					event: ['changed']
				},
				files: 'src/assets/js/**/*.js',
				tasks: 'jshint:js'
			}
		},

		jshint: {
			js: {
				src: ['src/assets/js/**/*.js']
			}
		}

	});

	// Load plugins

	//Copiar arquivos
	grunt.loadNpmTasks('grunt-contrib-copy');
	// Apagar arquivos
	grunt.loadNpmTasks('grunt-contrib-clean');
	grunt.loadNpmTasks('grunt-contrib-concat');
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-contrib-cssmin');
	grunt.loadNpmTasks('grunt-usemin');
	grunt.loadNpmTasks('grunt-contrib-imagemin');

	// versionamento de arquivos imagens versoes cache
	grunt.loadNpmTasks('grunt-rev');

	grunt.loadNpmTasks('grunt-contrib-sass');
	grunt.loadNpmTasks('grunt-contrib-watch');

	grunt.loadNpmTasks('grunt-contrib-jshint');
	grunt.loadNpmTasks('grunt-browser-sync');
	grunt.registerTask('server', ['browserSync', 'watch']);


	// Tasks
	grunt.registerTask('dist', ['clean', 'copy']); // apagar arquivos da dist e copiar novamente
	grunt.registerTask('minifica', ['useminPrepare',
		'concat', 'uglify', 'cssmin', 'rev:imagens', 'rev:minificados', 'usemin', 'imagemin'
	]);



	grunt.registerTask('default', ['dist', 'minifica']);


};